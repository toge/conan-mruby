from conans import ConanFile, tools
import shutil
import os

class MrubyConan(ConanFile):
    name           = "mruby"
    version        = "2.1.1"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-mruby"
    homepage       = "https://github.com/mruby/mruby" 
    description    = "<Description of Mruby here>"
    topics         = ("ruby", "mruby", "embedded")
    settings       = "os", "compiler", "build_type", "arch"
    generators     = "cmake"
    build_requires = ["ruby_installer/[>= 2.3.3]@bincrafters/stable", "bison_installer/3.3.2@bincrafters/stable"]
    exports        = "build_config.rb"

    def source(self):
        tools.get("https://github.com/mruby/mruby/archive/{}.zip".format(self.version))
        shutil.move("mruby-{}".format(self.version), "mruby")
        shutil.copyfile("build_config.rb", "mruby/build_config.rb")
        if self.settings.compiler == 'clang':
            tools.replace_in_file("mruby/build_config.rb", "toolchain :gcc", "toolchain :clang")

    def configure(self):
        self.options["libyaml"].shared = True

    def build(self):
        self.run("cd mruby; ruby ./minirake",  run_environment=True)

    def package(self):
        self.copy("*.h", dst="include", src="mruby/include")
        self.copy("*.lib", dst="lib", src="mruby/build/host", keep_path=False)
        self.copy("*.dll", dst="bin", src="mruby/build/host", keep_path=False)
        self.copy("*.so", dst="lib", src="mruby/build/host", keep_path=False)
        self.copy("*.dylib", dst="lib", src="mruby/build/host", keep_path=False)
        self.copy("*.a", dst="lib", src="mruby/build/host", keep_path=False)
        self.copy("mirb*", dst="bin", src="mruby/build/host/bin", keep_path=False)
        self.copy("mrbc*", dst="bin", src="mruby/build/host/bin", keep_path=False)
        self.copy("mruby*", dst="bin", src="mruby/build/host/bin", keep_path=False)
        self.copy("mruby-strip", dst="bin", src="mruby/build/host/bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["libmruby.a"]
        # self.cpp_info.libs = ["libmruby.a", "libmruby_core.a"]
        self.env_info.path.append(os.path.join(self.package_folder, "bin"))
